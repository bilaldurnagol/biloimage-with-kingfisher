//
//  BiloImageWithKingfisherApp.swift
//  BiloImageWithKingfisher
//
//  Created by Bilal Durnagöl on 9.09.2022.
//

import SwiftUI

@main
struct BiloImageWithKingfisherApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
