//
//  ContentView.swift
//  BiloImageWithKingfisher
//
//  Created by Bilal Durnagöl on 9.09.2022.
//

import SwiftUI

struct ContentView: View {
    var body: some View {
        List {
            ForEach(1..<100) {id in
               BiloImage(withPath: "https://picsum.photos/id/\(id)/200/\(4*id)")
            }
        }
        
    }
}


struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
